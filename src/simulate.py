#!/usr/bin/python
# Anil Ozdemir
# The University of Sheffield
# Final edit: Aug 2020

import numpy as np
import itertools, copy

PositionArray  = np.array([(0,0), (0,1), (-1,0), (0,-1), (1,0)]) # NoMove, N, W, S, E,  
Permute        = lambda Array, n: np.asarray(Array)[np.mod(np.arange(4) - n, 4)].tolist() # -n for CCW
SensorList     = [None] * 19
SensorList[0]  = tuple(map(list, itertools.product([0,2], repeat = 4))) # 0 Possible Direction. No Move. 
SensorList[1]  = tuple([[1,1,1,1]]) # 4 Possible Direction
SensorList[2]  = tuple(map(lambda i: Permute([1,1,1,0], i), range(4))) # 3 Possible Direction; 4 different config.
SensorList[3]  = tuple(map(lambda i: Permute([1,1,1,2], i), range(4))) # 3 Possible Direction; 4 different config.
SensorList[4]  = tuple(map(lambda i: Permute([2,1,2,1], i), range(2))) # 2 Possible Direction; 2 different config.
SensorList[5]  = tuple(map(lambda i: Permute([2,1,0,1], i), range(4))) # 2 Possible Direction; 4 different config.
SensorList[6]  = tuple(map(lambda i: Permute([0,1,0,1], i), range(2))) # 2 Possible Direction; 2 different config.
SensorList[7]  = tuple(map(lambda i: Permute([1,1,2,2], i), range(4))) # 2 Possible Direction; 4 different config.
SensorList[8]  = tuple(map(lambda i: Permute([1,1,0,2], i), range(4))) # 2 Possible Direction; 4 different config.
SensorList[9]  = tuple(map(lambda i: Permute([1,1,2,0], i), range(4))) # 2 Possible Direction; 4 different config.
SensorList[10] = tuple(map(lambda i: Permute([1,1,0,0], i), range(4))) # 2 Possible Direction; 4 different config.
SensorList[11] = tuple(map(lambda i: Permute([1,2,2,2], i), range(4))) # 1 Possible Direction; 4 different config.
SensorList[12] = tuple(map(lambda i: Permute([1,0,2,2], i), range(4))) # 1 Possible Direction; 4 different config.
SensorList[13] = tuple(map(lambda i: Permute([1,2,0,2], i), range(4))) # 1 Possible Direction; 4 different config.
SensorList[14] = tuple(map(lambda i: Permute([1,2,2,0], i), range(4))) # 1 Possible Direction; 4 different config.
SensorList[15] = tuple(map(lambda i: Permute([1,0,0,2], i), range(4))) # 1 Possible Direction; 4 different config.
SensorList[16] = tuple(map(lambda i: Permute([1,0,2,0], i), range(4))) # 1 Possible Direction; 4 different config.
SensorList[17] = tuple(map(lambda i: Permute([1,2,0,0], i), range(4))) # 1 Possible Direction; 4 different config.
SensorList[18] = tuple(map(lambda i: Permute([1,0,0,0], i), range(4))) # 1 Possible Direction; 4 different config.

UniqueSensors = np.array([
                        [1,1,1,1], 
                        [1,1,1,0], 
                        [1,1,1,2], 
                        [2,1,2,1], 
                        [2,1,0,1], 
                        [0,1,0,1], 
                        [1,1,2,2], 
                        [1,1,0,2], 
                        [1,1,2,0], 
                        [1,1,0,0], 
                        [1,2,2,2], 
                        [1,0,2,2], 
                        [1,2,0,2], 
                        [1,2,2,0], 
                        [1,0,0,2], 
                        [1,0,2,0], 
                        [1,2,0,0], 
                        [1,0,0,0]])

NumberLightActive = np.array([np.count_nonzero(item == 1) for item in UniqueSensors])
SpecialCaseIndex = [0,3,5] # Special Cases: A, C1, H1
NumberLightActive[SpecialCaseIndex] = 1 # Special cases has only 1 independent parameter.
NumberOfParameters = np.sum(NumberLightActive)

def ConstructDictionary():
    Dict = {}
    val_prev = 0
    for i, val in enumerate(NumberLightActive):
        Dict[i] = list(range(val_prev, val_prev+val))
        val_prev += val
    return Dict

SensorParDict = ConstructDictionary()

def FindSensorReadingIndex(iSensorReading):
    ''' 
       To find the location of the sensor reading.
       Example usage: FindSensorReadingIndex([2,2,0,2])
    '''
    for index, SensorReadings in enumerate(SensorList):
     try:
      return index, SensorReadings.index(iSensorReading)
     except Exception:
      pass

def BoundingBox(iPos, __nModule):
    '''
    Pos is Pos for 1 Cycle is array of (NMODULE,2).
    Return L_1, L_2, N_Hole = L_1 * L_2  - NMODULE
    '''
    L_x = np.max(iPos[:,0], axis = 0) - np.min(iPos[:,0], axis = 0) + 1
    L_y = np.max(iPos[:,1], axis = 0) - np.min(iPos[:,1], axis = 0) + 1
    L_1 = np.min([L_x, L_y]) 
    L_2 = np.max([L_x, L_y])
    return np.array([L_1, L_2, L_1 * L_2  - __nModule])

def UpdatePos(Pos_Prev, iRobot, iParameters):
    '''
    Input:  Pos_Prev: Pos at current time step.
            iRobot  : Index of the robot to move.
            iParameters: Parameter list [45x1]
    Process:
        - Update Sensor
        - Update Position
        - The order is N, W, S, E, so CCW.
        - Sensor: 0: Nothing, 1: Light, 2: Contact
    Output: Pos_Next: Pos for 1 cycle for the next time step.
    '''
    # Pos_Next = copy.copy(Pos_Prev)
    Pos_Next = Pos_Prev
    x, y     = Pos_Prev[iRobot]
    # Scan for contact.
    Neighbors      = [(x, y+1), (x-1, y), (x, y-1), (x+1, y)]
    Tuple_Pos_Next = set(map(tuple, Pos_Next))
    ContactSensor  = list(map(lambda Neighbor : Neighbor in Tuple_Pos_Next , Neighbors))
    Sensor         = list(map(lambda i: 2 if ContactSensor[i] else 1, range(4))) # Contact => 2, 1 otherwise. 
    # Scan for light.
    # Effect of each of this is about 40sec for 1000 modules run for 10000.
    Edge_North = np.max(Pos_Next[:,1], axis = 0)
    Edge_West  = np.min(Pos_Next[:,0], axis = 0)
    Edge_South = np.min(Pos_Next[:,1], axis = 0)
    Edge_East  = np.max(Pos_Next[:,0], axis = 0)
    if y == Edge_North:
        Sensor[0] = 0
    if x == Edge_West:
        Sensor[1] = 0
    if y == Edge_South:
        Sensor[2] = 0
    if x == Edge_East:
        Sensor[3] = 0
    # Sensor ops. 
    iUS, nPerm = FindSensorReadingIndex(Sensor)
    if iUS != 0:
        SensorRaw = list(map(lambda x: x if x==1 else 0, UniqueSensors[iUS-1].tolist()))
        IndexSensorRaw = np.where(np.array(SensorRaw) == 1)[0].tolist()
        IndexParameter = SensorParDict[iUS-1] # These parameters are bounded by 0 and 1.
        ParameterRaw = copy.copy(SensorRaw)
        # Considering the Special Cases!
        if iUS-1 in SpecialCaseIndex: # "-1" because starts from A, not N!
            LightSource = np.where(np.array(SensorRaw) == 1)[0]
            for iFace in LightSource: # The faces that have 1 inside
                ParameterRaw[iFace] = iParameters[IndexParameter][0]/LightSource.size # Assign 1 parameter that is equally distirbuted
        else:
            for i, par in enumerate(iParameters[IndexParameter]):
                ParameterRaw[IndexSensorRaw[i]] = par

        ActionProb = np.array(Permute(ParameterRaw, nPerm))
        Prob = np.insert(ActionProb, 0, len(IndexParameter)-np.sum(ActionProb))/len(IndexParameter) # (n_par - sum(par))/n_par
        # Randomly Choose where to move.
        DirToMove = np.random.choice(5, 1, p = Prob)[0] # 5 is len(Prob). 1 is to pick one in Prob.
        Pos_Next[iRobot] += PositionArray[DirToMove] 
    return Pos_Next

def Initialise(iTrial, nModule, size):
    np.random.seed(iTrial)
    Order = np.random.permutation(nModule)
    InitPos = set(map(tuple,np.random.randint(size, size = (nModule,2))))
    while (len(InitPos) != nModule):
        InitPos_Re = set(map(tuple,np.random.randint(size, size=(nModule-len(InitPos),2))))
        InitPos    = InitPos.union(InitPos_Re)    
    return [np.array(list(map(list,InitPos))), Order]

def Reformat(RawResults):
    # Extend the raw results (copy the final result to missing rows)
    RunTime = np.max(np.nonzero(RawResults[:,0])[0]) + 1 # NumberOfCyclesTook
    Results = np.copy(RawResults)
    Results[RunTime:] = RawResults[RunTime-1]
    return Results.astype(int)

def Simulate(Controller, iTrial, nRounds, nModule, size):
    # Return to Results unless it finishes early.
    # Seed is iTrial
    Pos_0, _Order = Initialise(iTrial, nModule, size)
    Results = np.zeros((nRounds, 3)) # 3 values for L_1, L_2, N_Hole
    for iCycle in range(nRounds):
        for iModule in _Order:
            Pos_1 = UpdatePos(Pos_0, iModule, Controller)
            Pos_0 = Pos_1
        Results[iCycle] = BoundingBox(Pos_1, nModule)
        # If condition is satisfied, reformat results and finish 
        if Results[iCycle, 2] < Results[iCycle, 0]:
            return Reformat(Results)
    return Results.astype(int)

def SimulateVisual(Controller, iTrial, nRounds, nModule, size):
    # Return to Results unless it finishes early.
    # Seed is iTrial
    Pos_0, _Order = Initialise(iTrial, nModule, size)
    Results = np.zeros((nRounds, 3)) # 3 values for L_1, L_2, N_Hole
    All_Pos = np.zeros((nRounds*nModule, nModule, 2))
    count   = 0
    for iCycle in range(nRounds):
        for iModule in _Order:
            Pos_1 = UpdatePos(Pos_0, iModule, Controller)
            Pos_0 = Pos_1
            All_Pos[count] = Pos_0
            count +=1
        Results[iCycle] = BoundingBox(Pos_1, nModule)
    return Results.astype(int), All_Pos
