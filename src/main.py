#!/usr/bin/python
# Anil Ozdemir
# The University of Sheffield
# Final edit: Aug 2020

from simulate import Simulate, SimulateVisual
from visualise import Visualiser
import numpy as np
import argparse
import time

parser = argparse.ArgumentParser(description='M-Blocks Stochastic Gathering')

parser.add_argument('--size'  , type=int , default=100  , metavar='s', help='arena size (bounded)')
parser.add_argument('--rounds', type=int , default=1000 , metavar='r', help='number of rounds')
parser.add_argument('--module', type=int , default=12   , metavar='m', help='number of modules')
parser.add_argument('--trial' , type=int , default=1    , metavar='t', help='trial ID (for random seed)')
parser.add_argument('--visual', type=bool, default=False, metavar='v', help='visual mode')
parser.add_argument('--save'  , type=bool, default=False, metavar='f', help='save animation as PNG')

args    = parser.parse_args()
SIZE    = args.size
NROUNDS = args.rounds
NMODULE = args.module
ITRIAL  = args.trial
VISUAL  = args.visual
SAVE    = args.save

Controller = np.load("Controller.npy")
start_time = time.time()

if not VISUAL:
    Results    = Simulate(Controller, ITRIAL, NROUNDS, NMODULE, SIZE)
    simTime    = time.time() - start_time
    print(">> Final bounding box is {}x{}. It took {:.3f} (s).".format(*Results[-1,:2], simTime))
else:
    Results, Pos = SimulateVisual(Controller, ITRIAL, NROUNDS, NMODULE, SIZE)
    simTime      = time.time() - start_time
    print(">> Final bounding box is {}x{}. It took {:.3f} (s).".format(*Results[-1,:2], simTime))
    vis = Visualiser(Pos, [SIZE, SIZE])
    if not SAVE:
        vis.AnimationPlot()
    else:  
        vis.SaveFig()



