#!/usr/bin/python
# Anil Ozdemir
# The University of Sheffield
# Final edit: Aug 2020

import numpy as np
from numpy import linalg as LA
import matplotlib.pyplot as P
from matplotlib.widgets import Slider, Button, RadioButtons
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import matplotlib.patches as patches
import copy, os
# import matplotlib
# matplotlib.use('Agg')

def SaveOperations(save):
    '''
    Handles the directory save operations. It creates the diredtory if does not exist.
    Usage : Do not start with '/' but end with '/'
    Input : save = (str) Path
    Output: void : creates a directory
    '''
    ImageName = save.split('/')[-1] # Extract Image name
    Directory = '/'.join(save.split('/')[:-1]) # Get Directory name
    if Directory and not os.path.exists(Directory): # If directory specified and does not exist, create it.
        os.makedirs(Directory)

class Visualiser:
    def __init__(self, Position, GridSize, Order = None, TimeFrame = None):
        '''
        Input: Grid is either a tuple or a list of Width and Height.
        '''
        self.Position = Position
        self.NumberOfRobots = Position.shape[1]
        self.GridWidth = GridSize[0]
        self.GridHeight = GridSize[1]
        self.Order = Order if np.sum(Order!=None)!=0 else np.arange(self.NumberOfRobots) 
        self.Time = TimeFrame if np.sum(TimeFrame!=None)!=0 else np.arange(len(Position)).astype("int")

        self.cRobot = []
        cmapjet = P.cm.get_cmap("jet")
        pallette = tuple(np.linspace(0, 1, self.NumberOfRobots))
        self.TextColour = []
        self.ModuleColours = []
        for iRobot in np.arange(self.NumberOfRobots): 
            colors = [(0,0,0,0), tuple(cmapjet(pallette)[iRobot])] # The first argument is the background, white.
            self.ModuleColours.append(colors[-1])
            self.cRobot.append(ListedColormap(colors))
            ColourNorm = LA.norm(colors[-1][:3])
            textcolour = 'w' if ColourNorm < 1.05 else 'k'
            self.TextColour.append(textcolour)
            
    def AnimationPlot(self, FrameRate = 1):
        # Animation Plot.
        Position       = copy.copy(self.Position)
        NumberOfRobots = copy.copy(self.NumberOfRobots)
        cRobot         = copy.copy(self.cRobot)
        fig, ax = P.subplots()
        iTimeStep = 0
        GridsToDisplay = np.zeros((NumberOfRobots, self.GridWidth, self.GridHeight))
        _GridToDisplay = []

        for iRobot in range(NumberOfRobots):
            GridsToDisplay[(iRobot,) + tuple(Position[iTimeStep, iRobot].astype(int))] = 1
            _GridToDisplay.append(ax.matshow(GridsToDisplay[iRobot].T, origin='lower', cmap=cRobot[iRobot])) # We take transpose as matshow works other way around. # Need to set origin as lower.
            if iRobot == self.Order[iTimeStep]:
                HatchRect = patches.Rectangle((Position[iTimeStep,iRobot,0]-0.5,Position[iTimeStep,iRobot,1]-0.5), 1, 1, color=self.TextColour[iRobot], hatch='//', clip_on = True, linewidth=0, fill=False, snap=False, alpha=0.5)
                Hatch = ax.add_patch(HatchRect)

        axcolor = 'lightgoldenrodyellow'
        axTimeStep = P.axes([0.26, 0.02, 0.60, 0.03], facecolor = axcolor)
        sliderTimeStep = Slider(axTimeStep, 'TimeStep', valmin = self.Time[0], valmax = self.Time[-1], valstep = FrameRate) #, valinit = 0, valfmt='%d')

        def update(val):
            iTimeStep = int(sliderTimeStep.val)
            GridsToDisplay = np.zeros((NumberOfRobots,self.GridWidth,self.GridHeight))  # We take transpose as matshow works other way around.
            for iRobot in range(NumberOfRobots):
                GridsToDisplay[(iRobot,) + tuple(Position[iTimeStep, iRobot].astype(int))] = 1 # One robot at a time, in different Grids.
                _GridToDisplay[iRobot].set_data(GridsToDisplay[iRobot].T)
                if iRobot == self.Order[iTimeStep%self.NumberOfRobots]:
                    Hatch.set_xy((Position[iTimeStep,iRobot,0]-0.5,Position[iTimeStep,iRobot,1]-0.5))
                    Hatch.set_color(self.TextColour[iRobot])
            sliderTimeStep.valtext.set_text("(" + str(np.floor(iTimeStep/NumberOfRobots).astype("int32")) + ", " + str(iTimeStep % NumberOfRobots) + ")")
            fig.canvas.draw_idle()

        resetax = P.axes([0.03, 0.02, 0.1, 0.03])
        button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')

        def reset(event):
            sliderTimeStep.reset()

        sliderTimeStep.on_changed(update)
        button.on_clicked(reset)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        P.show()

    def SaveFig(self, Hatch = False, DPI = 200, FrameRate = 1, savedir="./fig/"):
        # Save as Fig
        Position       = copy.copy(self.Position)
        NumberOfRobots = copy.copy(self.NumberOfRobots)
        cRobot         = copy.copy(self.cRobot)
        SaveOperations(savedir)
 
        for iTimeStep in self.Time:
            if (iTimeStep % 100) == 0: print(iTimeStep)
            fig, ax = P.subplots()
            fig.dpi = DPI
            GridsToDisplay = np.zeros((NumberOfRobots, self.GridWidth, self.GridHeight))
            for iRobot in range(NumberOfRobots):
                GridsToDisplay[(iRobot,) + tuple(Position[iTimeStep, iRobot].astype(int))] = 1
                _ = ax.matshow(GridsToDisplay[iRobot].T, origin='lower', cmap=cRobot[iRobot]) # We take transpose as matshow works other way around.
                if Hatch and iRobot == self.Order[iTimeStep % self.NumberOfRobots]:
                    ax.add_patch(patches.Rectangle((Position[iTimeStep,iRobot,0]-0.5,Position[iTimeStep,iRobot,1]-0.5), 1, 1, color=self.TextColour[iRobot], hatch='//', clip_on = True, linewidth=0, fill=False, snap=False, alpha=0.5))
            _ = ax.get_xaxis().set_ticks([]), ax.get_yaxis().set_ticks([])
            P.savefig(savedir + "/Frame-"+str(list(self.Time).index(iTimeStep)) + ".png", bbox_inches = 'tight')
        P.close()
