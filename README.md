# M-Blocks Simulator

M-Blocks Simulator. Currently, it has stochastic gathering simulator and visualiser implemented.

## Install

The source code only requires `numpy` and `matplotlib`. If prefer, one can run:
```bash
pip install -r requirements.txt
```

## Usage

File structure (`src`):
- `simulate.py` : simulates optimized stochastic m-blocks gathering
- `visualise.py`: if run with `simulate.SimulateVisual()`, one can animate or save simulation snapshots
- `main.py`     : includes parser to run the simulator in different modes

## Example run

In `src` directory, run:
```bash
python main.py --size 10 --module 12 --rounds 1000 --trial 1 --visual True
# The order of the arguments does not matter
# One can use the first letters instead, e.g. -s for --size
```

## Cite

- [Associated paper](http://aozdemir.net/publication/mrs2019/mrs2019.pdf)
- [Supplementary materials](https://doi.org/10.6084/m9.figshare.8527148)

```latex
@InProceedings{ozdemir2019decentralized,
  author    = {{\"O}zdemir, Anil and Romanishin, John W. and Gro{\ss}, Roderich and Rus, Daniela},
  title     = {Decentralized Gathering of Stochastic, Oblivious Agents on a Grid: A Case Study with 3D M-Blocks},
  booktitle = {2019 International Symposium on Multi-Robot and Multi-Agent Systems (MRS)},
  pages={245--251},
  year={2019},
  organization={IEEE}
}
```